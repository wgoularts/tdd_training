package br.com.peixeurbano

import org.bson.types.ObjectId
import org.joda.time.DateTime

case class Deal(
                 deal_id: String,
                 title: String,
                 buying_options: Seq[BuyingOption],
                 automatic_promocode: Option[String],
                 available_units: Int)

case class BuyingOption(
                         buying_option_id: String,
                         title: String,
                         sale_price: Double,
                         full_price: Double,
                         start: Option[DateTime],
                         end: Option[DateTime],
                         available_units: Int,
                         active: Boolean
                       )
