package br.com.peixeurbano

object CaixaEletronico {

  def sacar(valor: Int, res: (Int, Int, Int, Int) = (0, 0, 0, 0)): (Int, Int, Int, Int) = {
    valor match {
      case v if v >= 100 => sacar(valor - 100, (res._1 + 1, res._2, res._3, res._4))
      case v if v >= 50 => sacar(valor - 50, (res._1, res._2 + 1, res._3, res._4))
      case v if v >= 20 => sacar(valor - 20, (res._1, res._2, res._3 + 1, res._4))
      case v if v >= 10 => sacar(valor - 10, (res._1, res._2, res._3, res._4 + 1))
      case _ => res

    }
  }


}
