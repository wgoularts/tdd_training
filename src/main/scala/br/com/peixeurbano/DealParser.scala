package br.com.peixeurbano

trait DealParser {

  val dealsDocuments: DealsDocuments = null
  val promocodeDB: PromocodeDB = null
  val counter: DealCounter = null
  val dateHelper: DateHelper = null

  def findAndParse(dealId: String): Option[MainDeal] = {
    val deal = dealsDocuments.findById(dealId)
    deal.map(parseMainDeal)
  }

  private def parseMainDeal(deal: Deal): MainDeal = {
    val count = counter.getCounter(deal.deal_id)
    val dateMillis = dateHelper.now.getMillis
    val options = deal.buying_options.filter(op => {
      op.active && op.start.map(_.getMillis).getOrElse(dateMillis) <= dateMillis && op.end.map(_.getMillis).getOrElse(dateMillis) >= dateMillis
    }).map(bo => parseMainOptions(deal.deal_id, bo, count.map(_.option).getOrElse(Nil)))
    MainDeal(deal.deal_id, deal.title, options, options.head.sale_price, options.head.full_price, options.head.percent_discount,
      deal.automatic_promocode.flatMap(findPromocode), sold_out = count.exists(_.sold >= deal.available_units))
  }

  private def parseMainOptions(dealId: String, bopt: BuyingOption, count: Seq[CounterOption]): MainOption = {
    MainOption(bopt.buying_option_id, bopt.title, bopt.sale_price, bopt.full_price, (1.0 - bopt.sale_price / bopt.full_price) * 100,
      sold_out = count.find(_.buying_option_id == bopt.buying_option_id).exists(_.sold >= bopt.available_units))
  }

  private def findPromocode(promocode: String): Option[String] = {
    promocodeDB.findPromocode(promocode) match {
      case Some(Promocode(Some(valDiscount), _, _)) => Some("+ R$ %.2f de desconto".format(valDiscount))
      case Some(Promocode(_, Some(percentDiscount), _)) => Some("+ %s%s".format(percentDiscount, "%"))
      case _ => None
    }
  }
}

case class MainDeal(
                     deal_id: String,
                     title: String,
                     buying_options: Seq[MainOption],
                     min_sale_price: Double,
                     min_full_price: Double,
                     min_percent_discount: Double,
                     automatic_promocode_message: Option[String],
                     sold_out: Boolean)

case class MainOption(
                       buying_option_id: String,
                       title: String,
                       sale_price: Double,
                       full_price: Double,
                       percent_discount: Double,
                       sold_out: Boolean
                     )