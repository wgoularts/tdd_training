package br.com.peixeurbano

import org.joda.time.DateTime

trait DateHelper {

  def now: DateTime

}
