package br.com.peixeurbano

trait DealCounter {

  def getCounter(dealId: String): Option[Counter] = None

}
case class Counter(sold: Int, option: Seq[CounterOption])
case class CounterOption(buying_option_id: String, sold: Int)