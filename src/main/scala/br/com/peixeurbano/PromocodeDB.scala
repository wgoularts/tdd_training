package br.com.peixeurbano

class PromocodeDB {

  def findPromocode(promocode: String): Option[Promocode] = None

}

case class Promocode(value_discount: Option[Double], percent_discount: Option[Double], max_discount: Option[Double])