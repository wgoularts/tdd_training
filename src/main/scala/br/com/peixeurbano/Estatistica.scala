package br.com.peixeurbano



object Estatistica {

  def calcularEstatistica(nums: Int*): Option[EstatisticaResultado] = {

    def min(x: Int, y: Int): Int = {
      if (x > y) y
      else x
    }

    def max(x: Int, y: Int): Int = {
      if (x < y) y
      else x
    }

    if (nums.isEmpty) None
    else {

        var minimo = nums.head
        var maximo = nums.head
        var soma = 0

        for(n <- nums) {
          soma = soma + n
          minimo = min(minimo, n)
          maximo = max(maximo, n)
        }
        val tamanho = nums.size
        val avg  = soma / (tamanho * 1.0)
        Some(EstatisticaResultado(minimo, maximo, tamanho, avg))

    }


  }

}
