package br.com.peixeurbano

case class EstatisticaResultado(min: Int, max: Int, qtd: Int, avg: Double)
