package br.com.peixeurbano

import org.scalatest.FunSuite

class CaixaEletronicoTest extends FunSuite {

  test("Saque de 100 devolve nota de 100") {
    val result = CaixaEletronico.sacar(100)
    assert(result === (1,0,0,0))
  }

  test("Saque de 50 devolve 1 nota de 50") {
    val result = CaixaEletronico.sacar(50)
    assert(result === (0,1,0,0))
  }

  test("Saque de 20 devolve 1 nota de 20") {
    val result = CaixaEletronico.sacar(20)
    assert(result === (0,0,1,0))
  }

  test("Saque de 10 devolve 1 nota de 10") {
    val result = CaixaEletronico.sacar(10)
    assert(result === (0,0,0,1))
  }

  test("Saque de 150 devolve 1 nota de 100 e 1 note de 50") {
    val result = CaixaEletronico.sacar(150)
    assert(result === (1,1,0,0))
  }

  test("Saque de 180 devolve 1 nota de 100 e 1 note de 50 e 1 nota de 20 e 1 nota de 10") {
    val result = CaixaEletronico.sacar(180)
    assert(result === (1,1,1,1))
  }

  test("Saque de 190 devolve 1 nota de 100 e 1 nota de 50 e 2 notas de 20") {
    val result = CaixaEletronico.sacar(190)
    assert(result === (1,1,2,0))
  }

  test("Saque de 1770 devolve 17 nota de 100 e 1 nota de 50 e 1 nota de 20") {
    val result = CaixaEletronico.sacar(1770)
    assert(result === (17,1,1,0))
  }

}
