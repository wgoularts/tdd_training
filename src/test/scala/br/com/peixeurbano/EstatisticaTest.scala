package br.com.peixeurbano

import org.scalatest.FunSuite

class EstatisticaTest extends FunSuite{


  test("Estatistica de lista vazia"){
    val result = Estatistica.calcularEstatistica()
    assert(result === None)
  }

  test("Lista com um elemento") {
    val result = Estatistica.calcularEstatistica(1)
    assert(result === Some(EstatisticaResultado(1, 1, 1, 1)))
  }

  test("Lista com dois elementos"){
    val result = Estatistica.calcularEstatistica(1, 2)
    assert(result === Some(EstatisticaResultado(1, 2, 2, 1.5)))
  }

  test("Lista com dois elementos maiores que 2"){
    val result = Estatistica.calcularEstatistica(6, 3)
    assert(result === Some(EstatisticaResultado(3, 6, 2, 4.5)))
  }

  test("Lista com três elementos"){
    val result = Estatistica.calcularEstatistica(1, 2, 3)
    assert(result === Some(EstatisticaResultado(1, 3, 3, 2)))
  }

  test("Lista com um elemento diferente de 1") {
    val result = Estatistica.calcularEstatistica(2)
    assert(result === Some(EstatisticaResultado(2, 2, 1, 2)))
  }

  test("Lista com quatro elementos") {
    val result = Estatistica.calcularEstatistica(0, -1, 10, 21)
    assert(result === Some(EstatisticaResultado(-1, 21, 4, 7.5)))
  }

  test("Lista com 4 zeros") {
    val result = Estatistica.calcularEstatistica(0, 0, 0, 0)
    assert(result === Some(EstatisticaResultado(0, 0, 4, 0)))
  }


}
