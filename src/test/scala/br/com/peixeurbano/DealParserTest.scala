package br.com.peixeurbano

import org.joda.time.{DateTime, DateTimeZone}
import org.scalatest.{BeforeAndAfter, FunSuite}
import org.scalatest.mockito.MockitoSugar
import org.mockito.Mockito._
import org.mockito.Matchers._

class DealParserTest extends FunSuite with MockitoSugar with BeforeAndAfter {

  var dateHelperMock: DateHelper = _
  var dealsDocumentsMock: DealsDocuments = _
  var counterMock: DealCounter = _
  var promocodeDBMock: PromocodeDB = _
  var testing: DealParser = _

  val optionsAtiva  = BuyingOption("12", "Opcao", 4.5, 9.0, None, None, 10, active = true)
  val optionInativa  = BuyingOption("11", "Opcao Inativa", 4.5, 9.0, None, None, 10, active = false)
  val optionComData  = BuyingOption("11", "Opcao Com Data", 4.5, 9.0, Some(new DateTime(2017, 9, 5, 12, 30, DateTimeZone.UTC)), Some(new DateTime(2017, 9, 5, 18, 0, DateTimeZone.UTC)), 10, active = true)

  val dealUmaOpcaoAtiva = Deal("12345", "title", List(optionsAtiva), None, 10)
  val dealDuasOpcoesUmaAtiva = Deal("12345", "title", List(optionsAtiva, optionInativa), None, 10)
  val dealDuasOpcoesUmaComData = Deal("12345", "title", List(optionsAtiva, optionComData), None, 10)

  before {
    dealsDocumentsMock = mock[DealsDocuments]
    promocodeDBMock = mock[PromocodeDB]
    counterMock = mock[DealCounter]
    dateHelperMock = mock[DateHelper]

    testing = new DealParser {
      override val dealsDocuments: DealsDocuments = dealsDocumentsMock
      override val promocodeDB: PromocodeDB = promocodeDBMock
      override val counter: DealCounter = counterMock
      override val dateHelper: DateHelper = dateHelperMock
    }

    when(dealsDocumentsMock.findById(anyString())).thenReturn(None)
    when(counterMock.getCounter(anyString())).thenReturn(None)
    when(dateHelperMock.now).thenReturn(new DateTime(2017, 9, 5, 14, 0, DateTimeZone.UTC))
  }

  test("Parse de deal_id 123 inexistente") {
    val result = testing.findAndParse("123")
    verify(dealsDocumentsMock).findById("123")
    verify(promocodeDBMock, never()).findPromocode(anyString())
    verify(dateHelperMock, never()).now
    verify(counterMock, never()).getCounter(anyString())
    assert(None === result)
  }

  test("Parse de deal_id 12345 sem promocode") {
    val buyingOptions = Seq(MainOption("12", "Opcao", 4.5, 9.0, 50, sold_out = false))
    val mainDeal = MainDeal("12345", "title", buyingOptions, 4.5, 9.0, 50, None, sold_out = false)
    when(dealsDocumentsMock.findById("12345")).thenReturn(Some(dealUmaOpcaoAtiva))
    val result = testing.findAndParse("12345")
    verify(dealsDocumentsMock).findById("12345")
    verify(promocodeDBMock, never()).findPromocode(anyString())
    verify(counterMock).getCounter("12345")
    assert(Some(mainDeal) === result)
  }

  test("Parse de deal_id 12345 sem promocode esgotada") {
    val buyingOptions = Seq(MainOption("12", "Opcao", 4.5, 9.0, 50, sold_out = true))
    val mainDeal = MainDeal("12345", "title", buyingOptions, 4.5, 9.0, 50, None, sold_out = true)

    when(counterMock.getCounter("12345")).thenReturn(Some(Counter(12, Seq(CounterOption("12", 12)))))

    when(dealsDocumentsMock.findById("12345")).thenReturn(Some(dealUmaOpcaoAtiva))
    val result = testing.findAndParse("12345")
    verify(dealsDocumentsMock).findById("12345")
    verify(promocodeDBMock, never()).findPromocode(anyString())
    verify(counterMock).getCounter("12345")
    assert(Some(mainDeal) === result)
  }


  test("Parse de deal_id 12345 sem promocode nao esgotada") {
    val buyingOptions = Seq(MainOption("12", "Opcao", 4.5, 9.0, 50, sold_out = false))
    val mainDeal = MainDeal("12345", "title", buyingOptions, 4.5, 9.0, 50, None, sold_out = false)

    when(counterMock.getCounter("12345")).thenReturn(Some(Counter(2, Seq(CounterOption("12", 2)))))

    when(dealsDocumentsMock.findById("12345")).thenReturn(Some(dealUmaOpcaoAtiva))
    val result = testing.findAndParse("12345")
    verify(dealsDocumentsMock).findById("12345")
    verify(promocodeDBMock, never()).findPromocode(anyString())
    verify(counterMock).getCounter("12345")
    assert(Some(mainDeal) === result)
  }

  test("Parse de deal_id 12345 com opção inativa") {
    val buyingOptions = Seq(MainOption("12", "Opcao", 4.5, 9.0, 50, sold_out = false))
    val mainDeal = MainDeal("12345", "title", buyingOptions, 4.5, 9.0, 50, None, sold_out = false)

    when(counterMock.getCounter("12345")).thenReturn(Some(Counter(2, Seq(CounterOption("12", 2), CounterOption("11", 5)))))

    when(dealsDocumentsMock.findById("12345")).thenReturn(Some(dealDuasOpcoesUmaAtiva))
    val result = testing.findAndParse("12345")
    verify(dealsDocumentsMock).findById("12345")
    verify(promocodeDBMock, never()).findPromocode(anyString())
    verify(counterMock).getCounter("12345")
    assert(Some(mainDeal) === result)
  }

  test("Parse de deal_id 12345 com opção com a data fora da faixa") {
    val buyingOptions = Seq(MainOption("12", "Opcao", 4.5, 9.0, 50, sold_out = false))
    val mainDeal = MainDeal("12345", "title", buyingOptions, 4.5, 9.0, 50, None, sold_out = false)

    when(dateHelperMock.now).thenReturn(new DateTime(2017, 9, 5, 12, 29, DateTimeZone.UTC))

    when(counterMock.getCounter("12345")).thenReturn(Some(Counter(2, Seq(CounterOption("12", 2), CounterOption("11", 5)))))

    when(dealsDocumentsMock.findById("12345")).thenReturn(Some(dealDuasOpcoesUmaComData))
    val result = testing.findAndParse("12345")
    verify(dealsDocumentsMock).findById("12345")
    verify(promocodeDBMock, never()).findPromocode(anyString())
    verify(counterMock).getCounter("12345")
    verify(dateHelperMock).now
    assert(Some(mainDeal) === result)
  }

  test("Parse de deal_id 12345 com opção com a data dentro da faixa") {
    val buyingOptions = Seq(MainOption("12", "Opcao", 4.5, 9.0, 50, sold_out = false), MainOption("11", "Opcao Com Data", 4.5, 9.0, 50, sold_out = false))
    val mainDeal = MainDeal("12345", "title", buyingOptions, 4.5, 9.0, 50, None, sold_out = false)

    when(dateHelperMock.now).thenReturn(new DateTime(2017, 9, 5, 12, 35, DateTimeZone.UTC))

    when(counterMock.getCounter("12345")).thenReturn(Some(Counter(2, Seq(CounterOption("12", 2), CounterOption("11", 5)))))

    when(dealsDocumentsMock.findById("12345")).thenReturn(Some(dealDuasOpcoesUmaComData))
    val result = testing.findAndParse("12345")
    verify(dealsDocumentsMock).findById("12345")
    verify(promocodeDBMock, never()).findPromocode(anyString())
    verify(counterMock).getCounter("12345")
    verify(dateHelperMock).now
    assert(result === Some(mainDeal))
  }

  test("Parse de deal_id 12345 com opção com a data na faixa superior") {
    val buyingOptions = Seq(MainOption("12", "Opcao", 4.5, 9.0, 50, sold_out = false), MainOption("11", "Opcao Com Data", 4.5, 9.0, 50, sold_out = false))
    val mainDeal = MainDeal("12345", "title", buyingOptions, 4.5, 9.0, 50, None, sold_out = false)

    when(dateHelperMock.now).thenReturn(new DateTime(2017, 9, 5, 18, 0, DateTimeZone.UTC))

    when(counterMock.getCounter("12345")).thenReturn(Some(Counter(2, Seq(CounterOption("12", 2), CounterOption("11", 5)))))

    when(dealsDocumentsMock.findById("12345")).thenReturn(Some(dealDuasOpcoesUmaComData))
    val result = testing.findAndParse("12345")
    verify(dealsDocumentsMock).findById("12345")
    verify(promocodeDBMock, never()).findPromocode(anyString())
    verify(counterMock).getCounter("12345")
    verify(dateHelperMock).now
    assert(result === Some(mainDeal))
  }

  test("Parse de deal_id 12345 com opção com a data na faixa inferior") {
    val buyingOptions = Seq(MainOption("12", "Opcao", 4.5, 9.0, 50, sold_out = false), MainOption("11", "Opcao Com Data", 4.5, 9.0, 50, sold_out = false))
    val mainDeal = MainDeal("12345", "title", buyingOptions, 4.5, 9.0, 50, None, sold_out = false)

    when(dateHelperMock.now).thenReturn(new DateTime(2017, 9, 5, 12, 30, DateTimeZone.UTC))

    when(counterMock.getCounter("12345")).thenReturn(Some(Counter(2, Seq(CounterOption("12", 2), CounterOption("11", 5)))))

    when(dealsDocumentsMock.findById("12345")).thenReturn(Some(dealDuasOpcoesUmaComData))
    val result = testing.findAndParse("12345")
    verify(dealsDocumentsMock).findById("12345")
    verify(promocodeDBMock, never()).findPromocode(anyString())
    verify(counterMock).getCounter("12345")
    verify(dateHelperMock).now
    assert(result === Some(mainDeal))
  }

}
