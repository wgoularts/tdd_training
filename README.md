# Peixe Urbano - TDD - Desenvolvimento orientado a testes #

Treinamento interno para a prática de desenvolvimento orientado a testes (TDD)

### 2ª parte: Testando classes mais complexas, com dependências externas ###

* Usando Mockito: definir comportamentos das classes externas para testar o código da sua classe
* Vamos cobrir de testes o método `findAndParse(dealId: String)` da classe DealParser abaixo:

```scala
trait DealParser {

  val dealsDocuments: DealsDocuments
  val promocodeDB: PromocodeDB
  val counter: Counter
  val dateHelper: DateHelper

  def findAndParse(dealId: String): Option[MainDeal]
}
```

* Como podemos ver, esta classe possui 4 dependências externas:
  * DealsDocuments - responsável por buscar a Deal no MongoDB através de seu método `findById(dealId: String)`
  * PromocodeDB - responsável por buscar o promocode no banco de dados através do método `findPromocode(promocode: String)`, caso a Deal possua automatic_promocode definido, e caso exista, aplicá-lo nas opções da oferta
  * Counter - responsável por buscar no Redis os contadores de venda da oferta, através de seu método `getCounter(dealId: String)`
  * DateHelper - classe auxiliar que retorna a data corrente através de seu método `now`

* Deve-se cobrir de testes diversos possíveis cenários:
  * Ofertas podem ter 1 ou mais opções
  * As opções podem possuir data de entrada/saída
  * As ofertas ou opções podem ter esgotado
  * As ofertas podem possuir promocode automático
  * Promocodes podem ser por valor fixo ou percentual, e o percentual pode ter valor máximo a ser aplicado


### Coding dojo with TDD ###

* Tests first
* Baby steps
* Pair programming
* Time box (5 minutos)
* Regras:
  * Cada par tem 5 minutos para resolver um problema (ou parte dele), comporto por um piloto (teclado) e co-piloto
  * A linha de desenvolvimento/raciocínio deve ser incremental - baby steps:
    * Cria-se um teste para cobrir parte do algoritmo
    * Roda-se o teste (tem q *quebrar* o código)
    * Implementa-se o código até fazer passar este teste (e todos os testes anteriores)
    * Passou? Cria-se outro teste e assim sucessivamente
  * A platéia não interfere a não ser q seja necessário ou a pedido da dupla
  * Ao término dos 5 minutos, o piloto volta para a platéia, o co-piloto vira piloto e uma outra pessoa da plateia vira co-piloto


### Que problemas devemos resolver? ###

* Qualquer problema que seja resolvido por códigos pode (e DEVE) estar coberto por testes
* Separamos alguns simples pra praticarmos TDD:
  * Estatísticas de lista numérica
  * Cheque por extenso
  * Caixa eletrônico
  * Poker hand


### Estatísticas de lista numérica ###

Sua tarefa é processar uma seqüência de números inteiros para determinar as seguintes estatísticas:

* Valor mínimo
* Valor máximo
* Número de elementos na seqüência
* Valor médio

Por exemplo para uma seqüência de números "6, 9, 15, -2, 92, 11", temos como saída:

* Valor mínimo: -2
* Valor máximo: 92
* Número de elementos na seqüência: 6
* Valor médio: 18.1666666

DONE

### Cheque por extenso ###

Desenvolva um programa que dado um valor monetário, seja retornado o valor em reais por extenso.

Exemplo:

* 15415,16 -> quinze mil quatrocentos e quinze reais e dezesseis centavos
* 0,05 -> cinco centavos
* 2,25 -> dois reais e vinte e cinco centavos


### Caixa eletrônico ###

Desenvolva um programa que simule a entrega de notas quando um cliente efetuar um saque em um caixa eletrônico. Os requisitos básicos são os seguintes:

* Entregar o menor número de notas;
* É possível sacar o valor solicitado com as notas disponíveis;
* Saldo do cliente infinito;
* Quantidade de notas infinito (pode-se colocar um valor finito de cédulas para aumentar a dificuldade do problema);
* Notas disponíveis de R$ 100,00; R$ 50,00; R$ 20,00 e R$ 10,00

Exemplos:

* Valor do Saque: R$ 30,00 
  * Resultado Esperado: Entregar 1 nota de R$20,00 e 1 nota de R$ 10,00.
* Valor do Saque: R$ 80,00 
  * Resultado Esperado: Entregar 1 nota de R$50,00 1 nota de R$ 20,00 e 1 nota de R$ 10,00.
  
DONE


### Poker hand ###

* A poker deck contains 52 cards - each card has a suit which is one of clubs, diamonds, hearts, or spades (denoted C, D, H, and S in the input data).
* Each card also has a value which is one of 2, 3, 4, 5, 6, 7, 8, 9, 10, jack, queen, king, ace (denoted 2, 3, 4, 5, 6, 7, 8, 9, T, J, Q, K, A).
* For scoring purposes, the suits are unordered while the values are ordered as given above, with 2 being the lowest and ace the highest value.
* A poker hand consists of 5 cards dealt from the deck. Poker hands are ranked by the following partial order from lowest to highest.
* Ranking:
  * High Card: Hands which do not fit any higher category are ranked by the value of their highest card. If the highest cards have the same value, the hands are ranked by the next highest, and so on.
  * Pair: 2 of the 5 cards in the hand have the same value. Hands which both contain a pair are ranked by the value of the cards forming the pair. If these values are the same, the hands are ranked by the values of the cards not forming the pair, in decreasing order.
  * Two Pairs: The hand contains 2 different pairs. Hands which both contain 2 pairs are ranked by the value of their highest pair. Hands with the same highest pair are ranked by the value of their other pair. If these values are the same the hands are ranked by the value of the remaining card.
  * Three of a Kind: Three of the cards in the hand have the same value. Hands which both contain three of a kind are ranked by the value of the 3 cards.
  * Straight: Hand contains 5 cards with consecutive values. Hands which both contain a straight are ranked by their highest card.
  * Flush: Hand contains 5 cards of the same suit. Hands which are both flushes are ranked using the rules for High Card.
  * Full House: 3 cards of the same value, with the remaining 2 cards forming a pair. Ranked by the value of the 3 cards.
  * Four of a kind: 4 cards with the same value. Ranked by the value of the 4 cards.
  * Straight flush: 5 cards of the same suit with consecutive values. Ranked by the highest card in the hand.

* Your job is to rank pairs of poker hands and to indicate which, if either, has a higher rank.

Examples: 

* Input: Black: 2H 3D 5S 9C KD White: 2C 3H 4S 8C AH Output: White wins - high card: Ace
* Input: Black: 2H 4S 4C 2D 4H White: 2S 8S AS QS 3S Output: Black wins - full house
* Input: Black: 2H 3D 5S 9C KD White: 2C 3H 4S 8C KH Output: Black wins - high card: 9
* Input: Black: 2H 3D 5S 9C KD White: 2D 3H 5C 9S KH Output: Tie